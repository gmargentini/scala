package database.dao

import database.DatabaseProfile
import database.schema.RootSchema

import scala.concurrent.duration._

import scala.concurrent.Await

trait RootDao extends ProfissaoDao
{
  self:DatabaseProfile with RootSchema =>

  import dbConfig.profile.api._

  def checkAndCreateTable(): Unit = {
    val f = db.run(schema.create).recover{
      case e: Throwable => e.printStackTrace()
    }
    Await.ready(f, 20 seconds)
  }
}
