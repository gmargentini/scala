package database.dao

import database.DatabaseProfile
import database.schema.ProfissaoSchema
import models.Profissao

import scala.concurrent.Future

trait ProfissaoDao
{
  self: DatabaseProfile with ProfissaoSchema =>

  import dbConfig.profile.api._

  def findProfissaoByDescricao(descricao: String): Future[Seq[Profissao]] =
    db.run(profissoes.filter(_.descricao.like("%" + descricao + "%")).result )

  def findProfissaoById(id: Long): Future[Option[Profissao]] =
    db.run(profissoes.filter(_.id === id).result.headOption )

  def insertProfissao(profissao: Profissao): Future[Long] =
    db.run(profissoes returning profissoes.map(_.id) += profissao)

  def updateProfissao(profissao: Profissao): Future[Int] =
    profissao.id match {
      case None => Future.failed(new Exception("Id não informado"))
      case Some(id: Long) => db.run(profissoes.filter(_.id === id).update(profissao))
    }

  def deleteProfissao(id:Long): Future[Int] =
    db.run(profissoes.filter(_.id === id).delete)

}
