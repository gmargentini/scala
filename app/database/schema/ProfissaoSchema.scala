package database.schema

import database.DatabaseProfile
import models.Profissao

trait ProfissaoSchema
{
  self: DatabaseProfile =>

  import dbConfig.profile.api._

  class ProfissaoTable(tag: Tag) extends Table[Profissao] (tag, "profissao") {
    def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
    def descricao = column[String]("DESCRICAO", O.Unique)

    def * = (id.?, descricao).mapTo[Profissao]
  }

  val profissoes = TableQuery[ProfissaoTable]
}
