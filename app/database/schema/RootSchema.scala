package database.schema

import database.DatabaseProfile

trait RootSchema extends ProfissaoSchema {

  self: DatabaseProfile =>

  import dbConfig.profile.api._

  val schema = profissoes.schema

}
