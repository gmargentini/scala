package database

import slick.jdbc.JdbcProfile
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

trait DatabaseProfile
{
  def dbConfigProvider: DatabaseConfigProvider

  lazy val dbConfig = dbConfigProvider.get[JdbcProfile]
  def db = dbConfig.db

  implicit def ec: ExecutionContext

}