package database

import database.dao.RootDao
import database.schema.RootSchema
import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

class DataRepo @Inject()(val dbConfigProvider: DatabaseConfigProvider, val ec: ExecutionContext) extends DatabaseProfile with RootDao with RootSchema{

}