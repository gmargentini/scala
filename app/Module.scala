import com.google.inject.AbstractModule
import config.AppSetup

class Module extends AbstractModule{
  override def configure(): Unit = {
    bind(classOf[AppSetup]).asEagerSingleton()
  }
}
