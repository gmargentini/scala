package config

import database.DataRepo
import javax.inject._
import play.api.Logger

@Singleton
class AppSetup @Inject()(repo: DataRepo){
  Logger.info(message = "Criando tableas")
  repo.checkAndCreateTable()
}
