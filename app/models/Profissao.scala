package models

case class Profissao
(
  id: Option[Long],
  descricao: String
)
