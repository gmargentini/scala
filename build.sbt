name := """play-scala-seed"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

/*libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test*/

libraryDependencies ++= Seq(
  ws,
  guice,
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
  "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
  "org.sangria-graphql" %% "sangria" % "1.2.2",
  "org.sangria-graphql" %% "sangria-play-json" % "1.0.3",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % Test
)

PlayKeys.devSettings := Seq(
  "play.server.akka.requestTimeout" -> "3600s",
  "play.server.http.idleTimeout" -> "3600s",
  "akka.http.server.parsing.illegal-header-warnings" -> "off",
  "akka.http.client.parsing.illegal-header-warnings" -> "off",
  "akka.http.host-connection-pool.client.parsing.illegal-header-warnings" -> "off",
  "play.server.http.parsing.illegal-header-warnings" -> "off" )

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.example.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.example.binders._"
